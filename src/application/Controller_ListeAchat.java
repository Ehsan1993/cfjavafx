package application;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import connection.DatabaseConnection;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Achat;

public class Controller_ListeAchat implements Initializable {

	@FXML
	private Rectangle rectangleAnimation;

	@FXML
	private TableView<Achat> tableListeAchat;
	@FXML
	private TableColumn<Achat, String> dateAchat;
	@FXML
	private TableColumn<Achat, String> nomClientAchat;
	@FXML
	private TableColumn<Achat, String> prenomAchat;
	@FXML
	private TableColumn<Achat, String> secuAchat;
	@FXML
	private TableColumn<Achat, String> traitantAchat;
	@FXML
	private TableColumn<Achat, String> specialisteAchat;
	@FXML
	private TableColumn<Achat, String> mutuelleAchat;
	@FXML
	private TableColumn<Achat, String> medicamentAchat;
	@FXML
	private TableColumn<Achat, Integer> quantiteAchat;
	@FXML
	private TableColumn<Achat, Double> totalAchat;
	@FXML
	public ObservableList<Achat> data = FXCollections.observableArrayList();

	@FXML
	private RadioButton radioListeAchatJour;
	@FXML
	private RadioButton radioListeAchat;

	@FXML
	private AnchorPane monPanel;
	private Stage stage;
	private Scene scene;
	private Parent root;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		ToggleGroup tg = new ToggleGroup();
		radioListeAchat.setToggleGroup(tg);
		radioListeAchatJour.setToggleGroup(tg);

		TranslateTransition translate = new TranslateTransition();
		translate.setNode(rectangleAnimation);
		translate.setDuration(Duration.millis(8000));
		translate.setCycleCount(TranslateTransition.INDEFINITE);
		translate.setByX(1300);
		translate.play();
	}

	public void radiobutton() {

		if (radioListeAchat.isSelected()) {
			data.clear();

			Connection con;
			try {
				con = DatabaseConnection.getConnection();
				String sql = "SELECT * FROM achat a LEFT JOIN  ordonnance ord ON a.id_Achat = ord.id_Achat"
						+ " LEFT JOIN clients c ON a.id_Client = c.id_Client"
						+ " LEFT JOIN liste_medicament lm ON a.id_Achat = lm.id_Achat"
						+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
						+ " LEFT JOIN mutuelle mu ON mu.id_Mutuelle = a.id_Mutuelle"
						+ " JOIN medecin me ON me.id_Medecin = c.id_Medecin"
						+ " LEFT JOIN specialiste sp ON ord.id_Specialiste = sp.id_Specialiste";

				PreparedStatement stat = con.prepareStatement(sql);
				ResultSet rs = stat.executeQuery();

				while (rs.next()) {

					String nomClient = rs.getString("NomClient");
					String prenomClient = rs.getString("PrenomClient");
					String secuClient = rs.getString("Securite_Sociale");
					String medecinClient = rs.getString("NomMedecin");
					String specialisteClient = rs.getString("NomSpecialiste");
					String mutuelleClient = rs.getString("NomMutuelle");
					String medicamentClient = rs.getString("NomMedicament");
					String dateAchatClient = rs.getString("dateAchat");
					Integer quMedicClient = rs.getInt("quantite_Medicament");

					Double remboursement = rs.getDouble("Remboursement");
					Double prix = rs.getDouble("PrixMedicament");

					Double total = (prix - ((remboursement / 100) * prix)) * quMedicClient;

					data.add(new Achat(0, nomClient, prenomClient, secuClient, medecinClient,
							dateAchatClient, specialisteClient, medicamentClient, mutuelleClient,
							quMedicClient, 0.0, 0.0, total));
				}
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			dateAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("dateAchat"));
			nomClientAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("nomClient"));
			prenomAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("prenomClient"));
			secuAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("secu"));
			traitantAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("medecin"));
			mutuelleAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("mutuelle"));
			medicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("medicament"));
			quantiteAchat.setCellValueFactory(
					new PropertyValueFactory<Achat, Integer>("quantiteMedicament"));
			totalAchat.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prixTotal"));
			specialisteAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("specialiste"));

			tableListeAchat.setItems(data);

		} else if (radioListeAchatJour.isSelected()) {

			data.clear();

			Connection con;
			try {
				con = DatabaseConnection.getConnection();
				String sql = "SELECT * FROM achat a"
						+ " LEFT JOIN  ordonnance ord ON a.id_Achat = ord.id_Achat"
						+ " LEFT JOIN clients c ON a.id_Client = c.id_Client"
						+ " LEFT JOIN liste_medicament lm ON a.id_Achat = lm.id_Achat"
						+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
						+ " LEFT JOIN mutuelle mu ON mu.id_Mutuelle = a.id_Mutuelle"
						+ " JOIN medecin me ON me.id_Medecin = c.id_Medecin"
						+ " LEFT JOIN specialiste sp ON ord.id_Specialiste = sp.id_Specialiste WHERE dateAchat = CURDATE() ";

				PreparedStatement stat = con.prepareStatement(sql);
				ResultSet rs = stat.executeQuery();

				while (rs.next()) {

					String nomClient = rs.getString("NomClient");
					String prenomClient = rs.getString("PrenomClient");
					String secuClient = rs.getString("Securite_Sociale");
					String medecinClient = rs.getString("NomMedecin");
					String specialisteClient = rs.getString("NomSpecialiste");
					String mutuelleClient = rs.getString("NomMutuelle");
					String medicamentClient = rs.getString("NomMedicament");
					String dateAchatClient = rs.getString("dateAchat");
					Integer quMedicClient = rs.getInt("quantite_Medicament");

					Double remboursement = rs.getDouble("Remboursement");
					Double prix = rs.getDouble("PrixMedicament");

					Double total = (prix - ((remboursement / 100) * prix)) * quMedicClient;

					data.add(new Achat(0, nomClient, prenomClient, secuClient, medecinClient,
							dateAchatClient, specialisteClient, medicamentClient, mutuelleClient,
							quMedicClient, 0.0, 0.0, total));
				}
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			dateAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("dateAchat"));
			nomClientAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("nomClient"));
			prenomAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("prenomClient"));
			secuAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("secu"));
			traitantAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("medecin"));
			specialisteAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("specialiste"));
			mutuelleAchat.setCellValueFactory(new PropertyValueFactory<Achat, String>("mutuelle"));
			medicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("medicament"));
			quantiteAchat.setCellValueFactory(
					new PropertyValueFactory<Achat, Integer>("quantiteMedicament"));
			totalAchat.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prixTotal"));

			tableListeAchat.setItems(data);
		}
	}

	public void pageAccueil(ActionEvent event) throws IOException {

		root = FXMLLoader.load(getClass().getResource("Main.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void buttonQuitter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.exit(0);
		}
	}
}