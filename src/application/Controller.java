package application;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import connection.DatabaseConnection;
import exceptions.MissMatchException;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Achat;
import model.Client;
import model.Medecin;
import model.Medicament;
import model.Mutuelle;
import model.Ordonnance;
import model.Specialiste;

public class Controller implements Initializable {

	@FXML
	private Button buttonListeClientPA, buttonAchatPA, enregistrerClient, effacerClient,
			buttonValider, buttonAjouter, buttonSupprimer, buttonBack, listeClient;
	@FXML
	private TextField tfNom, tfPrenom, tfVille, tfCodePostal, tfAdresse, tfPhone, tfMail,
			tfSecuAchat, tfSecuriteSociale;
	@FXML
	private Label labelPrincipale, labelQuantite, labelNom, labelMedicament, labelSpecialiste,
			labelMutuelle, labelSecurite, titreAchatOrdo, titreAchatSansOrdo, labelTypeAchat,
			dateJour, dateJour1, dateJour2, dateJour3;
	@FXML
	private DatePicker datePicker;
	@FXML
	private ComboBox<String> comboBoxTypeAchat;
	@FXML
	private ComboBox<Medicament> comboMedicament;
	@FXML
	private ComboBox<Specialiste> comboSpecialiste;
	@FXML
	private ComboBox<Mutuelle> comboMutuelleAchat;
	@FXML
	private ComboBox<Mutuelle> choiceBoxMutuelle;
	@FXML
	private ComboBox<Medecin> choiceBoxTraitant;
	@FXML
	private ComboBox<Client> comboNom;
	@FXML
	private ComboBox<Medecin> comboMedecin2;
	@FXML
	private Spinner<Integer> spinnerQuantite;
	private SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(
			1, 100);

	@FXML
	private Rectangle animation;

	@FXML
	private TableView<Ordonnance> table_Ordonnance;
	@FXML
	private TableColumn<Ordonnance, String> patient;
	@FXML
	private TableColumn<Ordonnance, String> dateOrdonnance;
	@FXML
	private TableColumn<Ordonnance, String> nomSpecialiste;
	@FXML
	private TableColumn<Ordonnance, String> listeMedicament;
	@FXML
	private TableView<Achat> tableAchat;
	@FXML
	private TableColumn<Achat, String> nomMedicamentAchat;
	@FXML
	private TableColumn<Achat, Double> prixMedicamentAchat;
	@FXML
	private TableColumn<Achat, Double> remboursementMedicament;
	@FXML
	private TableColumn<Achat, Integer> quantiteMedicamentAchat;
	@FXML
	private TableColumn<Achat, Double> totalAchat;

	@FXML
	public ObservableList<Ordonnance> listeOrdonnance = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Integer> listeIdAchat = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Achat> listeAchatPourIDs = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Achat> listeAchat = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Mutuelle> listeMutuelle = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Mutuelle> listeMutuelleCombo = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Medecin> listeMedecin = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Client> listeDeClient = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Specialiste> listeSpecialiste = FXCollections.observableArrayList();
	@FXML
	public ObservableList<Medicament> listeMedic = FXCollections.observableArrayList();

	@FXML
	private AnchorPane monPanel;
	private Stage stage;
	private Scene scene;
	private Parent root;

	Connection con;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		// Remplir le comboBox Mutuelle de SQL
		try {
			con = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM mutuelle";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				Integer idMutuelle = rs.getInt("id_Mutuelle");
				String nomMutuelle = rs.getString("NomMutuelle");
				String ville = rs.getString("Ville");
				String postal = rs.getString("CodePostal");
				String adresse = rs.getString("Adresse");
				String phone = rs.getString("Phone");
				String mail = rs.getString("Email");
				String departement = rs.getString("Departement");
				Double remb = rs.getDouble("Remboursement");
				String remboursement = String.valueOf(remb);

				listeMutuelle.add(new Mutuelle(idMutuelle, nomMutuelle, departement, ville, postal,
						adresse, phone, mail, remboursement));
			}
			choiceBoxMutuelle.getItems().addAll(listeMutuelle);
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Remplir le comboBox Medecin en SQL
		try {
			con = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM Medecin";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idMedecin = rs.getInt("id_Medecin");
				String nomMedecin = rs.getString("NomMedecin");
				String prenomMedecin = rs.getString("Prenom");
				String villeMedecin = rs.getString("Ville");
				String codePostalMedecin = rs.getString("CodePostal");
				String adresseMedecin = rs.getString("Adresse");
				String phoneMedecin = rs.getString("Phone");
				String mailMedecin = rs.getString("Email");
				String numAgreementMedecin = rs.getString("numAgreement");

				listeMedecin.add(new Medecin(idMedecin, nomMedecin, prenomMedecin, villeMedecin,
						codePostalMedecin, adresseMedecin, phoneMedecin, mailMedecin,
						numAgreementMedecin));
			}
			choiceBoxTraitant.getItems().addAll(listeMedecin);
			comboMedecin2.getItems().addAll(listeMedecin);
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Remplir le comboBox de client de SQL
		try {
			con = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM clients c JOIN mutuelle m ON c.id_Mutuelle = m.id_Mutuelle JOIN medecin mt ON c.id_Medecin = mt.id_Medecin";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idClient = rs.getInt("id_Client");
				String nomClient = rs.getString("NomClient");
				String prenomClient = rs.getString("PrenomClient");
				String naissanceClient = rs.getString("Date_Naissance");
				String villeClient = rs.getString("Ville");
				String codePostalClient = rs.getString("Code_Postal");
				String adresseClient = rs.getString("Adresse");
				String phoneClient = rs.getString("Phone");
				String mailClient = rs.getString("Mail");
				String numSecuClient = rs.getString("Securite_Sociale");
				String mutuelleClient = rs.getString("NomMutuelle");
				String traitantClient = rs.getString("NomMedecin");

				listeDeClient.add(new Client(idClient, nomClient, prenomClient, naissanceClient,
						villeClient, codePostalClient, adresseClient, phoneClient, mailClient,
						numSecuClient, mutuelleClient, traitantClient));
			}
			comboNom.getItems().addAll(listeDeClient);
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Remplir le comboBox de Medicament de SQL

		try {

			con = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM medicament";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idMedicament = rs.getInt("id_Medicament");
				String nomMedicament = rs.getString("NomMedicament");
				String categorie = rs.getString("CategorieMedicament");
				String dateService = rs.getString("DateService");
				Double prixMedicament = rs.getDouble("PrixMedicament");
				String quantiteMedicament = String.valueOf(rs.getInt("QuantiteMedicament"));

				String prix = String.valueOf(prixMedicament);

				listeMedic.add(new Medicament(idMedicament, nomMedicament, categorie, dateService,
						prix, quantiteMedicament));

			}
			comboMedicament.getItems().addAll(listeMedic);
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Remplir le comboBox de Specialiste de SQL
		try {
			con = DatabaseConnection.getConnection();
			String sql = "SELECT* FROM specialiste";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {

				Integer idSpecialiste = rs.getInt("id_Specialiste");
				String nomSpecialiste = rs.getString("NomSpecialiste");
				String prenomSpecialiste = rs.getString("Prenom");
				String villeSpecialiste = rs.getString("Ville");
				String codePostalSpecialiste = rs.getString("CodePostal");
				String adresseSpecialiste = rs.getString("Adresse");
				String phoneSpecialiste = rs.getString("Phone");
				String mailSpecialiste = rs.getString("Email");
				String specialite = rs.getString("Specialite");

				listeSpecialiste.add(new Specialiste(idSpecialiste, nomSpecialiste,
						prenomSpecialiste, villeSpecialiste, codePostalSpecialiste,
						adresseSpecialiste, phoneSpecialiste, mailSpecialiste, specialite));
			}
			comboSpecialiste.getItems().addAll(listeSpecialiste);
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Définir la date de jour
		LocalDate today = LocalDate.now();
		String formattedDate = today.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
		dateJour.setText(formattedDate.toString());
		dateJour1.setText(formattedDate.toString());
		dateJour2.setText(formattedDate.toString());
		dateJour3.setText(formattedDate.toString());

		// Animation du titre d'application "Sparadrap"
		FadeTransition fade = new FadeTransition();
		fade.setNode(labelPrincipale);
		fade.setDuration(Duration.millis(15000));
		fade.setFromValue(0);
		fade.setToValue(1);
		fade.play();

		// Animation derrière l'application
		TranslateTransition translate = new TranslateTransition();
		translate.setNode(animation);
		translate.setDuration(Duration.millis(10000));
		translate.setCycleCount(TranslateTransition.INDEFINITE);
		translate.setAutoReverse(true);
		translate.setByX(1200);
		translate.play();

		// Remplir le combo box type d'achat
		comboBoxTypeAchat.getSelectionModel().select(" ");
		comboBoxTypeAchat.getItems().add("Via Ordonnance");
		comboBoxTypeAchat.getItems().add("Sans Ordonnance");

		// Remplir le spinner pour la quantité des médicaments
		valueFactory.setValue(1);
		spinnerQuantite.setValueFactory(valueFactory);

	}

	public void typeAchat() {
		if (comboBoxTypeAchat.getValue() != null
				&& comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {

			tableAchat.setVisible(true);
			comboNom.setVisible(true);
			comboMedicament.setVisible(true);
			comboSpecialiste.setVisible(true);
			labelQuantite.setVisible(true);
			labelNom.setVisible(true);
			labelMedicament.setVisible(true);
			labelSpecialiste.setVisible(true);
			labelMutuelle.setVisible(true);
			labelSecurite.setVisible(true);
			tfSecuAchat.setVisible(true);
			comboMutuelleAchat.setVisible(true);
			spinnerQuantite.setVisible(true);
			titreAchatOrdo.setVisible(true);
			buttonValider.setVisible(true);
			buttonAjouter.setVisible(true);
			buttonSupprimer.setVisible(true);
			buttonBack.setVisible(true);

			comboBoxTypeAchat.setVisible(false);
			labelTypeAchat.setVisible(false);

		} else if (comboBoxTypeAchat.getValue() != null
				&& comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {

			tableAchat.setVisible(true);
			comboNom.setVisible(true);
			comboMedicament.setVisible(true);
			labelQuantite.setVisible(true);
			labelNom.setVisible(true);
			labelMedicament.setVisible(true);
			labelSecurite.setVisible(true);
			tfSecuAchat.setVisible(true);
			spinnerQuantite.setVisible(true);
			titreAchatSansOrdo.setVisible(true);
			buttonValider.setVisible(true);
			buttonAjouter.setVisible(true);
			buttonSupprimer.setVisible(true);
			buttonBack.setVisible(true);

			comboSpecialiste.setVisible(false);
			labelSpecialiste.setVisible(false);
			labelMutuelle.setVisible(false);
			comboMutuelleAchat.setVisible(false);
			comboBoxTypeAchat.setVisible(false);
			labelTypeAchat.setVisible(false);

		}
	}

	public void comboNom() {

		Client clientCombo = comboNom.getValue();
		if (clientCombo != null) {
			tfSecuAchat.setText(clientCombo.getNumSecuriteSociale());
			String mutuelleNom = clientCombo.getMutuelle();

			if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")
					|| comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {
				comboMutuelleAchat.getItems().clear();
				try {
					con = DatabaseConnection.getConnection();
					String sql = "SELECT * FROM clients c JOIN mutuelle m ON c.id_Mutuelle = m.id_Mutuelle WHERE NomMutuelle LIKE"
							+ " \'" + mutuelleNom + "\'LIMIT 1";

					PreparedStatement stat = con.prepareStatement(sql);
					ResultSet rs = stat.executeQuery();

					while (rs.next()) {

						Integer idMutuelle = rs.getInt("id_Mutuelle");
						String nomMutuelle = rs.getString("NomMutuelle");
						String ville = rs.getString("Ville");
						String postal = rs.getString("CodePostal");
						String adresse = rs.getString("Adresse");
						String phone = rs.getString("Phone");
						String mail = rs.getString("Email");
						String departement = rs.getString("Departement");
						Double remb = rs.getDouble("Remboursement");
						String remboursement = String.valueOf(remb);

						listeMutuelleCombo.add(new Mutuelle(idMutuelle, nomMutuelle, departement,
								ville, postal, adresse, phone, mail, remboursement));
						comboMutuelleAchat.getItems().addAll(listeMutuelleCombo);
					}
					con.close();

					comboMutuelleAchat.getSelectionModel().select(0);
					listeMutuelleCombo.clear();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void buttonBack(ActionEvent event) {

		tableAchat.setVisible(false);
		comboNom.setVisible(false);
		comboMedicament.setVisible(false);
		comboSpecialiste.setVisible(false);
		labelQuantite.setVisible(false);
		labelNom.setVisible(false);
		labelMedicament.setVisible(false);
		labelSpecialiste.setVisible(false);
		labelMutuelle.setVisible(false);
		labelSecurite.setVisible(false);
		tfSecuAchat.setVisible(false);
		comboMutuelleAchat.setVisible(false);
		spinnerQuantite.setVisible(false);
		titreAchatOrdo.setVisible(false);
		titreAchatSansOrdo.setVisible(false);
		buttonValider.setVisible(false);
		buttonAjouter.setVisible(false);
		buttonSupprimer.setVisible(false);
		buttonBack.setVisible(false);

		comboNom.getSelectionModel().clearSelection();
		comboSpecialiste.getSelectionModel().clearSelection();
		comboMedicament.getSelectionModel().clearSelection();
		comboMutuelleAchat.getItems().clear();
		comboBoxTypeAchat.getSelectionModel().clearSelection();
		tfSecuAchat.setText(null);
		valueFactory.setValue(1);

		comboBoxTypeAchat.setVisible(true);
		labelTypeAchat.setVisible(true);

		listeAchatPourIDs.clear();
		listeAchat.clear();

	}

	public void buttonQuitter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			stage = (Stage) monPanel.getScene().getWindow();
			stage.close();
		}
	}

	public void buttonListeClientPA() {
		buttonListeClientPA.toFront();
		buttonListeClientPA.setPrefSize(150, 150);
		buttonListeClientPA.relocate(75, 25);
		buttonListeClientPA.setText("Liste des clients");
		buttonListeClientPA.setFont(new Font("Times New Roman", 18));

	}

	public void buttonAchatPA() {
		buttonAchatPA.toFront();
		buttonAchatPA.setPrefSize(150, 150);
		buttonAchatPA.relocate(175, 25);
		buttonAchatPA.setText("Liste d'achat");
		buttonAchatPA.setFont(new Font("Times New Roman", 18));
	}

	public void buttonExit() {

		buttonListeClientPA.setPrefSize(100, 100);
		buttonAchatPA.setPrefSize(100, 100);

		buttonListeClientPA.relocate(100, 50);
		buttonAchatPA.relocate(200, 50);

		buttonListeClientPA.setFont(new Font("Times New Roman", 12));
		buttonAchatPA.setFont(new Font("Times New Roman", 12));

		buttonListeClientPA.setText("Liste client");
		buttonAchatPA.setText("Achat");

	}

	public void listeAchat(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("FenetreListeAchat.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void listeClient(ActionEvent event) throws IOException {

		root = FXMLLoader.load(getClass().getResource("FenetreListeClient.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	public void infoClient(ActionEvent event) throws IOException {

		Parent root = FXMLLoader.load(getClass().getResource("FenetreInfoClient.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void buttonEnregistrer() {

		try {

			Integer idClient = 0;
			String nom = tfNom.getText();
			String prenom = tfPrenom.getText();

			LocalDate date = datePicker.getValue();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String naissance = date.format(formatter);

			String ville = tfVille.getText();
			String codePostal = tfCodePostal.getText();
			String adresse = tfAdresse.getText();
			String phone = tfPhone.getText();
			String mail = tfMail.getText();
			String secu = tfSecuriteSociale.getText();
			Mutuelle mutuelle = choiceBoxMutuelle.getValue();
			Medecin medecin = choiceBoxTraitant.getValue();

			Client client = new Client(idClient, nom, prenom, naissance, ville, codePostal, adresse,
					phone, mail, secu, String.valueOf(mutuelle.getId_Mutuelle()),
					String.valueOf(medecin.getId_Medecin()));

			con = DatabaseConnection.getConnection();

			String sql = "INSERT INTO clients (NomClient, PrenomClient, Date_Naissance, Ville, Code_Postal, Adresse, Phone, Mail, Securite_Sociale, id_Mutuelle, id_Medecin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement stat = con.prepareStatement(sql);

			stat.setString(1, client.getNom());
			stat.setString(2, client.getPrenom());
			stat.setString(3, client.getDateDeNaissance());
			stat.setString(4, client.getVille());
			stat.setString(5, client.getCodePostal());
			stat.setString(6, client.getAdresse());
			stat.setString(7, client.getPhone());
			stat.setString(8, client.getMail());
			stat.setString(9, client.getNumSecuriteSociale());
			stat.setInt(10, Integer.parseInt(client.getMutuelle()));
			stat.setInt(11, Integer.parseInt(client.getMedecinTraitant()));

			stat.executeUpdate();
			stat.close();
			con.close();

		} catch (MissMatchException e2) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(e2.getMessage());
			alert.setContentText(e2.getMessage());
			alert.showAndWait();
		} catch (NumberFormatException e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(e1.getMessage());
			alert.setContentText(e1.getMessage());
			alert.showAndWait();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException NPE) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Manque d'information!!!");
			alert.setContentText("Veuillez remplir les champs!!!");
			alert.showAndWait();
		}
	}

	public void buttonAjouterMedicament() {

		try {
			Client client = comboNom.getValue();
			Medicament medicament = comboMedicament.getValue();
			Specialiste specialiste = comboSpecialiste.getValue();

			Integer quantiteMedic = spinnerQuantite.getValue();
			Double prixMedic = medicament.getPrix();

			LocalDate today = LocalDate.now();
			String dateAchat = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

			if (comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {

				// Objet l'achat pour la liste d'Achat
				listeAchat.add(new Achat(0, client.getNom(), client.getPrenom(),
						client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
						"non", medicament.getNom(), "non", quantiteMedic, prixMedic, 0.0,
						prixMedic * quantiteMedic));
				// Objet Achat pour la liste des médicaments
				listeAchatPourIDs.add(new Achat(0, client.getNom(), client.getPrenom(),
						client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
						"non", medicament.getIdMedicament().toString(), "non", quantiteMedic,
						prixMedic, 0.0, prixMedic * quantiteMedic));

			} else if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {

				Mutuelle mutuelle = comboMutuelleAchat.getValue();
				Double rembourse = mutuelle.getRemboursement();
				Double prixTotalOrdonnance = (prixMedic - ((rembourse / 100) * prixMedic))
						* quantiteMedic;

				if (comboSpecialiste.getValue() == null) {
					// Objet l'achat pour la liste d'Achat
					listeAchat.add(new Achat(0, client.getNom(), client.getPrenom(),
							client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
							"non", medicament.getNom(), mutuelle.getNom(), quantiteMedic, prixMedic,
							rembourse, prixTotalOrdonnance));
					// Objet Achat pour la liste des médicaments
					listeAchatPourIDs.add(new Achat(0, client.getNom(), client.getPrenom(),
							client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
							"non", medicament.getIdMedicament().toString(), mutuelle.getNom(),
							quantiteMedic, prixMedic, rembourse, prixTotalOrdonnance));
				} else {

					// Objet l'achat pour la liste d'Achat
					listeAchat.add(new Achat(0, client.getNom(), client.getPrenom(),
							client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
							specialiste.getNom(), medicament.getNom(), mutuelle.getNom(),
							quantiteMedic, prixMedic, rembourse, prixTotalOrdonnance));
					// Objet Achat pour la liste des médicaments
					listeAchatPourIDs.add(new Achat(0, client.getNom(), client.getPrenom(),
							client.getNumSecuriteSociale(), client.getMedecinTraitant(), dateAchat,
							specialiste.getNom(), medicament.getIdMedicament().toString(),
							mutuelle.getNom(), quantiteMedic, prixMedic, rembourse,
							prixTotalOrdonnance));
				}
			}

			nomMedicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, String>("medicament"));
			prixMedicamentAchat
					.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prix"));
			remboursementMedicament
					.setCellValueFactory(new PropertyValueFactory<Achat, Double>("remboursement"));
			quantiteMedicamentAchat.setCellValueFactory(
					new PropertyValueFactory<Achat, Integer>("quantiteMedicament"));
			totalAchat.setCellValueFactory(new PropertyValueFactory<Achat, Double>("prixTotal"));

			tableAchat.setItems(listeAchat);
		} catch (NullPointerException npe) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERROR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez remplir les champs ?");
			alert.showAndWait();
		}
		comboMedicament.getSelectionModel().clearSelection();
		valueFactory.setValue(1);
	}

	public void buttonValiderAchat() {

		ObservableList<Achat> items = tableAchat.getItems();
		if (items.size() > 0) {
			Client client = comboNom.getValue();

			Integer idClient = client.getIdClient();
			LocalDate today = LocalDate.now();
			String dateAchat = today.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

			// Enregistrer l'achat dans la liste d'achat à la base de donnée

			try {
				con = DatabaseConnection.getConnection();
				String sql = "INSERT INTO achat (dateAchat, id_Client, id_Mutuelle) VALUES (?, ?, ?)";
				PreparedStatement stat = con.prepareStatement(sql);

				stat.setString(1, dateAchat);
				stat.setInt(2, idClient);

				if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {
					Mutuelle mutuelle = comboMutuelleAchat.getValue();
					Integer mutuelleNom = mutuelle.getId_Mutuelle();

					stat.setInt(3, mutuelleNom);

				} else if (comboBoxTypeAchat.getValue().equals("Sans Ordonnance")) {
					stat.setString(3, null);
				}

				stat.executeUpdate();
				stat.close();
				con.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			// Récupérer id_Achat de la base de donnée

			try {
				con = DatabaseConnection.getConnection();
				String sql = "SELECT max(id_Achat) FROM achat WHERE id_Client LIKE" + "\'"
						+ idClient + "\'";
				PreparedStatement stat = con.prepareStatement(sql);
				ResultSet rs = stat.executeQuery();

				while (rs.next()) {

					Integer idAchat = rs.getInt(1);
					listeIdAchat.add(idAchat);
				}
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			// Remplir la table Ordonnance en base de donnée

			if (comboBoxTypeAchat.getValue().equals("Via Ordonnance")) {
				try {

					Integer idAchat = listeIdAchat.get(0);
					con = DatabaseConnection.getConnection();
					String sql = "INSERT INTO ordonnance (dateOrdonnance, id_Achat, id_Specialiste) VALUES (?, ?, ?)";

					PreparedStatement stat = con.prepareStatement(sql);

					if (comboSpecialiste.getValue() != null) {
						Specialiste specialiste = comboSpecialiste.getValue();
						Integer idSpecialiste = specialiste.getIdSpecialiste();
						stat.setInt(3, idSpecialiste);
					} else {
						stat.setInt(3, 0);
					}
					stat.setString(1, dateAchat);
					stat.setInt(2, idAchat);

					stat.executeUpdate();
					stat.close();
					con.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			// Remplir la liste médicament en base de donnée

			for (Achat achat : listeAchatPourIDs) {

				dateAchat = achat.getDateAchat();
				Integer quantite = achat.getQuantiteMedicament();
				Integer idMedi = Integer.parseInt(achat.getMedicament());

				try {
					Integer idAchat = listeIdAchat.get(0);
					con = DatabaseConnection.getConnection();
					String sql = "INSERT INTO liste_medicament (id_Achat, id_Medicament, quantite_Medicament) VALUES (?, ?, ?)";

					PreparedStatement stat = con.prepareStatement(sql);

					stat.setInt(1, idAchat);
					stat.setInt(2, idMedi);
					stat.setInt(3, quantite);

					stat.executeUpdate();
					stat.close();
					con.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			listeAchatPourIDs.clear();
			listeIdAchat.clear();
			comboMedicament.getSelectionModel().clearSelection();
			comboNom.getSelectionModel().clearSelection();
			comboMutuelleAchat.getSelectionModel().clearSelection();
			comboSpecialiste.getSelectionModel().clearSelection();
			tfSecuAchat.setText(null);
			valueFactory.setValue(1);
			tableAchat.getItems().clear();
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERROR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez choisir les médicaments d'abord ?");
			alert.showAndWait();
		}
	}

	public void buttonSupprimer() {
		try {
			int selectedIndex = tableAchat.getSelectionModel().getSelectedIndex();
			tableAchat.getItems().remove(selectedIndex);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERROR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez remplir le tableau d'abord ?");
			alert.showAndWait();

		}
	}

	public void buttonEffacer() {

		tfNom.setText("");
		tfPrenom.setText("");
		tfVille.setText("");
		tfCodePostal.setText("");
		tfPhone.setText("");
		tfMail.setText("");
		tfSecuriteSociale.setText("");
		tfAdresse.setText("");
		datePicker.setValue(null);
		choiceBoxMutuelle.setValue(null);
		choiceBoxTraitant.setValue(null);
	}

	public void listeOrdonnance() {
		table_Ordonnance.getItems().clear();
		try {
			Connection con;
			con = DatabaseConnection.getConnection();
			String sql = "SELECT NomClient, NomSpecialiste, dateOrdonnance, o.id_Achat, group_concat(NomMedicament) as listeMedicaments"
					+ " FROM ordonnance o JOIN achat a ON a.id_Achat = o.id_Achat"
					+ " JOIN clients c ON c.id_Client = a.id_Client"
					+ " JOIN liste_medicament lm ON lm.id_Achat = a.id_Achat"
					+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
					+ " LEFT JOIN specialiste s ON o.id_Specialiste = s.id_Specialiste GROUP BY lm.id_Achat";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				String nomPatient = rs.getString("NomClient");
				String dateOrdonnance = rs.getString("dateOrdonnance");
				String specialiste = rs.getString("NomSpecialiste");
				Integer idAchat = rs.getInt("id_Achat");
				String listeMedicament = rs.getString("listeMedicaments");

				listeOrdonnance.add(new Ordonnance(nomPatient, dateOrdonnance, idAchat, specialiste,
						listeMedicament));
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		patient.setCellValueFactory(new PropertyValueFactory<Ordonnance, String>("client"));
		dateOrdonnance.setCellValueFactory(
				new PropertyValueFactory<Ordonnance, String>("dateOrdonnance"));
		nomSpecialiste.setCellValueFactory(
				new PropertyValueFactory<Ordonnance, String>("nomSpecialiste"));
		listeMedicament.setCellValueFactory(
				new PropertyValueFactory<Ordonnance, String>("listeMedicament"));

		table_Ordonnance.setItems(listeOrdonnance);
	}

	public void listeOrdonnanceParMedecin() {
		try {
			table_Ordonnance.getItems().clear();
			Integer idMedecin = comboMedecin2.getValue().getId_Medecin();
			try {
				Connection con;
				con = DatabaseConnection.getConnection();
				String sql = "SELECT NomClient, NomSpecialiste, dateOrdonnance, o.id_Achat, group_concat(NomMedicament) as listeMedicaments"
						+ " FROM ordonnance o JOIN achat a ON a.id_Achat = o.id_Achat"
						+ " JOIN clients c ON c.id_Client = a.id_Client"
						+ " JOIN liste_medicament lm ON lm.id_Achat = a.id_Achat"
						+ " JOIN medicament m ON m.id_Medicament = lm.id_Medicament"
						+ " LEFT JOIN specialiste s ON o.id_Specialiste = s.id_Specialiste WHERE id_Medecin = "
						+ "\'" + idMedecin + "'GROUP BY lm.id_Achat";

				PreparedStatement stat = con.prepareStatement(sql);
				ResultSet rs = stat.executeQuery();

				while (rs.next()) {

					String nomPatient = rs.getString("NomClient");
					String dateOrdonnance = rs.getString("dateOrdonnance");
					String specialiste = rs.getString("NomSpecialiste");
					Integer idAchat = rs.getInt("id_Achat");
					String listeMedicament = rs.getString("listeMedicaments");

					listeOrdonnance.add(new Ordonnance(nomPatient, dateOrdonnance, idAchat,
							specialiste, listeMedicament));
				}

				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			patient.setCellValueFactory(new PropertyValueFactory<Ordonnance, String>("client"));
			dateOrdonnance.setCellValueFactory(
					new PropertyValueFactory<Ordonnance, String>("dateOrdonnance"));
			nomSpecialiste.setCellValueFactory(
					new PropertyValueFactory<Ordonnance, String>("nomSpecialiste"));
			listeMedicament.setCellValueFactory(
					new PropertyValueFactory<Ordonnance, String>("listeMedicament"));

			table_Ordonnance.setItems(listeOrdonnance);
		} catch (NullPointerException NPE) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ERROR");
			alert.setHeaderText("Manque d'information!");
			alert.setContentText("Veuillez choisir un médecin!!!");
			alert.showAndWait();
		}
	}
}