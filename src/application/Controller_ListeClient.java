package application;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ResourceBundle;

import connection.DatabaseConnection;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Client;

public class Controller_ListeClient implements Initializable {

	@FXML
	private Rectangle rectangleAnimation;

	@FXML
	private TableView<Client> tableListeClient;
	@FXML
	private TableColumn<Client, String> nom;
	@FXML
	private TableColumn<Client, String> prenom;
	@FXML
	private TableColumn<Client, LocalDate> naissance;
	@FXML
	private TableColumn<Client, String> ville;
	@FXML
	private TableColumn<Client, String> postal;
	@FXML
	private TableColumn<Client, String> adresse;
	@FXML
	private TableColumn<Client, String> phone;
	@FXML
	private TableColumn<Client, String> mail;
	@FXML
	private TableColumn<Client, String> secu;
	@FXML
	private TableColumn<Client, String> mutuelle;
	@FXML
	private TableColumn<Client, String> traitant;
	@FXML
	public ObservableList<Client> data = FXCollections.observableArrayList();

	@FXML
	private AnchorPane monPanel;
	private Stage stage;
	private Scene scene;
	private Parent root;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		TranslateTransition translate = new TranslateTransition();
		translate.setNode(rectangleAnimation);
		translate.setDuration(Duration.millis(8000));
		translate.setCycleCount(TranslateTransition.INDEFINITE);
		translate.setByX(1300);
		translate.play();

		Connection con;

		try {
			con = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM clients c JOIN mutuelle m ON c.id_Mutuelle = m.id_Mutuelle JOIN medecin mt ON c.id_Medecin = mt.id_Medecin";
			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet rs = stat.executeQuery();

			while (rs.next()) {

				Integer idClient = rs.getInt("id_Client");
				String nomSQL = rs.getString("NomClient");
				String prenomSQL = rs.getString("PrenomClient");
				String naissanceSQL = rs.getString("Date_Naissance");
				String villeSQL = rs.getString("Ville");
				String postalSQL = rs.getString("Code_Postal");
				String adresseSQL = rs.getString("Adresse");
				String phoneSQL = rs.getString("Phone");
				String mailSQL = rs.getString("Mail");
				String secuSQL = rs.getString("Securite_Sociale");
				String mutuelleSQL = rs.getString("NomMutuelle");
				String medecinSQL = rs.getString("NomMedecin");

				data.add(new Client(idClient, nomSQL, prenomSQL, naissanceSQL, villeSQL, postalSQL,
						adresseSQL, phoneSQL, mailSQL, secuSQL, mutuelleSQL, medecinSQL));
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		nom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
		prenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
		naissance.setCellValueFactory(
				new PropertyValueFactory<Client, LocalDate>("dateDeNaissance"));
		ville.setCellValueFactory(new PropertyValueFactory<Client, String>("ville"));
		postal.setCellValueFactory(new PropertyValueFactory<Client, String>("codePostal"));
		adresse.setCellValueFactory(new PropertyValueFactory<Client, String>("adresse"));
		phone.setCellValueFactory(new PropertyValueFactory<Client, String>("phone"));
		mail.setCellValueFactory(new PropertyValueFactory<Client, String>("mail"));
		secu.setCellValueFactory(new PropertyValueFactory<Client, String>("numSecuriteSociale"));
		mutuelle.setCellValueFactory(new PropertyValueFactory<Client, String>("mutuelle"));
		traitant.setCellValueFactory(new PropertyValueFactory<Client, String>("medecinTraitant"));

		tableListeClient.setItems(data);
		// tableListeClient.getItems().addAll(data);
	}

	public void pageAccueil(ActionEvent event) throws IOException {

		root = FXMLLoader.load(getClass().getResource("Main.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	public void buttonQuitter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Quitter");
		alert.setHeaderText("Vous êtes en train de sortir du programme!");
		alert.setContentText("Vous êtes sûre de vouloir quitter?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.exit(0);
//			stage = (Stage) monPanel.getScene().getWindow();
//			stage.close();
		}
	}
}
