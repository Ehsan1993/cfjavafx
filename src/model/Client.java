package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import exceptions.MissMatchException;

public class Client extends Personne {

	private LocalDate dateDeNaissance;
	private String numSecuriteSociale;
	private String mutuelle;
	private String medecinTraitant;
	private Integer idClient;

	public Client(Integer idClient, String nom, String prenom, String dateNaissanceClient,
			String ville, String codePostal, String adresse, String phone, String mail,
			String numSecuriteSociale, String mutuelle, String medecinTraitant)
			throws MissMatchException {
		super(nom, prenom, ville, codePostal, adresse, phone, mail);
		setIdClient(idClient);
		setDateDeNaissance(dateNaissanceClient);
		setNumSecuriteSociale(numSecuriteSociale);
		setMutuelle(mutuelle);
		setMedecinTraitant(medecinTraitant);
	}

	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getDateDeNaissance() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return dateDeNaissance.format(formatter);
	}

	public void setDateDeNaissance(String dateDeNaissance) {
		if (dateDeNaissance == null || dateDeNaissance.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir la date de naissance");
		} else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dataNaissance = LocalDate.parse(dateDeNaissance, formatter);
			this.dateDeNaissance = dataNaissance;
		}
	}

	public String getNumSecuriteSociale() {
		return numSecuriteSociale;
	}

	public void setNumSecuriteSociale(String numSecuriteSociale) throws MissMatchException {
		if (numSecuriteSociale == null || numSecuriteSociale.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir le numéro de Sécurité Sociale!");
		}
		if (!Pattern.matches("[1|2][0-9]{14}", numSecuriteSociale)) {
			throw new MissMatchException(
					"Un numero de securite sociale contient forcement 15 chiffres et commence par 1 ou 2");

		} else {
			this.numSecuriteSociale = numSecuriteSociale;
		}
	}

	public String getMutuelle() {
		return mutuelle;
	}

	public void setMutuelle(String mutuelleString) throws MissMatchException {
//		if (mutuelleString == null) {
//			throw new NumberFormatException("Veuillez choisir le mutuelle!");
//		} else {
		this.mutuelle = mutuelleString;
	}
//	}

	public String getMedecinTraitant() {
		return medecinTraitant;
	}

	public void setMedecinTraitant(String medecinTraitant) throws NumberFormatException {
//		if (medecinTraitant == null) {
//			throw new NumberFormatException("Veuillez choisir le médecin!");
//		} else {
		this.medecinTraitant = medecinTraitant;
//		}
	}

	@Override
	public String toString() {
		return this.getNom() + " " + this.getPrenom();
	}
}
