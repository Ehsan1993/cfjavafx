package model;

import java.util.regex.Pattern;

import exceptions.MissMatchException;

public class Medecin extends Personne {
	private Integer id_Medecin;
	private String numAgreement;

	public Medecin(Integer id_Medecin, String nom, String prenom, String ville, String codePostal,
			String adresse, String phone, String mail, String numAgreement)
			throws MissMatchException {
		super(nom, prenom, ville, codePostal, adresse, phone, mail);
		setId_Medecin(id_Medecin);
		setNumAgreement(numAgreement);
	}

	public Integer getId_Medecin() {
		return id_Medecin;
	}

	public void setId_Medecin(Integer id_Medecin) {
		this.id_Medecin = id_Medecin;
	}

	public String getNumAgreement() {
		return numAgreement;
	}

	public void setNumAgreement(String numAgreement) throws MissMatchException {

		if (!Pattern.matches("[0-9]{11}", numAgreement)) {
			throw new MissMatchException("Un numéro d'agrément contient forcement 11 chiffres!");
		} else {
			this.numAgreement = numAgreement;
		}
	}

	@Override
	public String toString() {
		return "Dr. " + this.getNom();
	}
}
