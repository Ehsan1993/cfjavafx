package model;

public class Ordonnance {

	private String client;
	private String dateOrdonnance;
	private Integer idAchat;
	private String nomSpecialiste;
	private String listeMedicament;

	public Ordonnance(String client, String dateOrdonnance, Integer idAchat, String nomSpecialiste,
			String listeMedicament) {
		setClient(client);
		setDateOrdonnance(dateOrdonnance);
		setIdAchat(idAchat);
		setNomSpecialiste(nomSpecialiste);
		setListeMedicament(listeMedicament);
	}

	public String getListeMedicament() {
		return listeMedicament;
	}

	public void setListeMedicament(String listeMedicament) {
		this.listeMedicament = listeMedicament;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getDateOrdonnance() {
		return dateOrdonnance;
	}

	public void setDateOrdonnance(String dateOrdonnance) {
		this.dateOrdonnance = dateOrdonnance;
	}

	public Integer getIdAchat() {
		return idAchat;
	}

	public void setIdAchat(Integer idAchat) {
		this.idAchat = idAchat;
	}

	public String getNomSpecialiste() {
		return nomSpecialiste;
	}

	public void setNomSpecialiste(String nomSpecialiste) {
		this.nomSpecialiste = nomSpecialiste;
	}
}
