package model;

import java.util.regex.Pattern;

import exceptions.MissMatchException;

public class Mutuelle {

	private Integer id_Mutuelle;
	private String nom;
	private String ville;
	private String codePostal;
	private String adresse;
	private String phone;
	private String mail;
	private String departement;
	private Double remboursement;

	public Mutuelle(Integer id_Mutuelle, String nom, String departement, String ville,
			String codePostal, String adresse, String phone, String mail, String remboursement)
			throws MissMatchException {
		setId_Mutuelle(id_Mutuelle);
		setNom(nom);
		setDepartement(departement);
		setVille(ville);
		setCodePostal(codePostal);
		setAdresse(adresse);
		setPhone(phone);
		setMail(mail);
		setRemboursement(remboursement);
	}

	public Integer getId_Mutuelle() {
		return id_Mutuelle;
	}

	public void setId_Mutuelle(Integer id_Mutuelle) {
		this.id_Mutuelle = id_Mutuelle;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) throws MissMatchException {
		this.nom = nom;
	}

	public String getVille() {
		return ville;
	}

	private void setVille(String ville) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôï]*)+", ville)) {
			throw new MissMatchException("Le nom de ville n'est pas bon. Insérez un nom valide!");
		} else {
			this.ville = ville;
		}
	}

	public String getCodePostal() {
		return codePostal;
	}

	private void setCodePostal(String codePostal) throws MissMatchException {
		if (!Pattern.matches("[1-9][0-9]{4}", codePostal)) {
			throw new MissMatchException(
					"Le code postal n'est pas bon. Un code postal est forcement 5 chiffres!");
		} else {
			this.codePostal = codePostal;
		}
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhone() {
		return phone;
	}

	private void setPhone(String phone) throws MissMatchException {
		if (!Pattern.matches("^(\\+33|0|0033)[1-9](\\d{2}){4}$", phone)) {
			throw new MissMatchException(
					"Le numéro de téléphone n'est pas bon. Insérez un numéro valide!");
		} else {
			this.phone = phone;
		}
	}

	public String getMail() {
		return mail;
	}

	private void setMail(String mail) throws MissMatchException {
		if (!Pattern.matches(
				"^[a-zA-Z0-9]+[-._]*[a-zA-Z0-9]+@[a-zA-Z0-9]+[-]*[a-zA-Z0-9]*.[a-zA-Z0-9]+$",
				mail)) {
			throw new MissMatchException(
					"L'adresse mail n'est pas bon. Insérez une adresse mail valide!");
		} else {
			this.mail = mail;
		}
	}

	public String getDepartement() {
		return departement;
	}

	private void setDepartement(String departement) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-é èç&àîêôï]*)+", departement)) {
			throw new MissMatchException(
					"Le nom de Département n'est pas bon. Insérez un nom valide!");
		} else {
			this.departement = departement;
		}
	}

	public Double getRemboursement() {
		return remboursement;
	}

	private void setRemboursement(String remboursement) throws MissMatchException {
		if (!Pattern.matches("[1-9][0-9]*\\.[0-9]{1,2}", remboursement)) {
			throw new MissMatchException(
					"Le remboursement n'est pas bon. Insérez un chiffre valide!");
		} else {
			this.remboursement = Double.parseDouble(remboursement);
		}
	}

	@Override
	public String toString() {
		return this.getNom();
	}
}
