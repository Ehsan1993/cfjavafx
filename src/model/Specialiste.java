package model;

import java.util.regex.Pattern;

import exceptions.MissMatchException;

public class Specialiste extends Personne {

	private String specialite;
	private Integer idSpecialiste;

	public Specialiste(Integer idSpecialiste, String nom, String prenom, String ville,
			String codePostal, String adresse, String phone, String mail, String specialite)
			throws MissMatchException {
		super(nom, prenom, ville, codePostal, adresse, phone, mail);
		setSpecialite(specialite);
		setIdSpecialiste(idSpecialiste);
	}

	public Integer getIdSpecialiste() {
		return idSpecialiste;
	}

	public void setIdSpecialiste(Integer idSpecialiste) {
		this.idSpecialiste = idSpecialiste;
	}

	public String getSpecialite() {
		return specialite;
	}

	private void setSpecialite(String specialite) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-'éèçàâîêôï]*)+", specialite)) {
			throw new MissMatchException("La specialité n'est pas bonne!");
		} else {
			this.specialite = specialite;
		}
	}

	public String toString() {
		return "Dr. " + this.getNom() + " " + this.getPrenom();
	}
}
