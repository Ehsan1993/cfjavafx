package model;

import java.util.regex.Pattern;

import exceptions.MissMatchException;

public abstract class Personne {

	private String nom;
	private String prenom;
	private String codePostal;
	private String ville;
	private String adresse;
	private String phone;
	private String mail;

	public Personne(String nom, String prenom, String ville, String codePostal, String adresse,
			String phone, String mail) throws MissMatchException {
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setCodePostal(codePostal);
		this.setAdresse(adresse);
		this.setVille(ville);
		this.setPhone(phone);
		this.setMail(mail);
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getVille() {
		return ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public String getAdresse() {
		return adresse;
	}

	public String getPhone() {
		return phone;
	}

	public String getMail() {
		return mail;
	}

	public void setNom(String nom) throws MissMatchException {
		if (nom == null || nom.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir un nom!");
		}

		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôï]*)+", nom)) {
			throw new MissMatchException("Le nom n'est pas bon. Insérez un nom valide!");
		} else {

			this.nom = nom.toUpperCase();
		}
	}

	public void setPrenom(String prenom) throws MissMatchException {
		if (prenom == null || prenom.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir un prénom!");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôï]*)+", prenom)) {
			throw new MissMatchException("Le prénom n'est pas bon. Insérez un prénom valide!");
		} else {

			this.prenom = prenom;
		}
	}

	public void setCodePostal(String codePostal) throws MissMatchException {
		if (codePostal == null || codePostal.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir le code postal!");
		}
		if (!Pattern.matches("[1-9][0-9]{4}", codePostal)) {
			throw new MissMatchException(
					"Le code postal n'est pas bon. Un code postal a forcement 5 chiffres!");
		} else {
			this.codePostal = codePostal;
		}

	}

	public void setVille(String ville) throws MissMatchException {
		if (ville == null || ville.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir le nom de la ville!");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôï]*)+", ville)) {
			throw new MissMatchException("Le nom de ville n'est pas bon. Insérez un nom valide!");
		} else {
			this.ville = ville;
		}
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public void setPhone(String phone) throws MissMatchException {
		if (phone == null || phone.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir un numéro de téléphone!");
		}
		if (!Pattern.matches("^(\\+33|0|0033)[1-9](\\d{2}){4}$", phone)) {
			throw new MissMatchException(
					"Le numéro de téléphone n'est pas bon. Insérez un numéro valide!");
		} else {
			this.phone = phone;
		}
	}

	public void setMail(String mail) throws MissMatchException {
		if (mail == null || mail.trim().isEmpty()) {
			throw new NumberFormatException("Veuillez saisir l'adresse mail!");
		}
		if (!Pattern.matches(
				"^[a-zA-Z0-9]+[-._]*[a-zA-Z0-9]+@[a-zA-Z0-9]+[-]*[a-zA-Z0-9]*.[a-zA-Z0-9]+$",
				mail)) {
			throw new MissMatchException(
					"L'adresse mail n'est pas bon. Insérez une adresse mail valide!");
		} else {
			this.mail = mail;
		}
	}
}
