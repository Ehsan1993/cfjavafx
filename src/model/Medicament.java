package model;

import java.time.LocalDate;
import java.util.regex.Pattern;

import exceptions.MissMatchException;

public class Medicament {

	private Integer idMedicament;
	private String nomMedicament;
	private String categorie;
	private Double prix;
	private LocalDate dateService;
	private Integer quantite;

	public Medicament(Integer idMedicament, String nomMedicament, String categorie,
			String dateService, String prix, String quantite) throws MissMatchException {

		setIdMedicament(idMedicament);
		setNom(nomMedicament);
		setCategorie(categorie);
		setPrix(prix);
		setDateService(dateService);
		setQuantite(quantite);

	}

	public Integer getIdMedicament() {
		return idMedicament;
	}

	public void setIdMedicament(Integer idMedicament) {
		this.idMedicament = idMedicament;
	}

	public String getNom() {
		return nomMedicament;
	}

	public void setNom(String nom) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-'éèçàîêôï]*)+", nom))
			throw new MissMatchException("Le nom médicament n'est pas bon. Insérez un nom valide!");
		this.nomMedicament = nom;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) throws MissMatchException {
		if (!Pattern.matches("([a-zA-Z]+[-'éèçàîêôï]*)+", categorie))
			throw new MissMatchException(
					"la catagorie de médicament pas bonne. Insérez un nom valide!");
		this.categorie = categorie;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(String prix) throws MissMatchException {
		if (!Pattern.matches("[1-9][0-9]*\\.[0-9]{1,2}", prix))
			throw new MissMatchException("Le prix n'est pas bon. Insérez un prix valide!");
		this.prix = Double.parseDouble(prix);
	}

	public LocalDate getDateService() {
		return dateService;
	}

	public void setDateService(String dateService) {
		LocalDate date = LocalDate.parse(dateService);
		this.dateService = date;
	}

	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) throws MissMatchException {
		if (!Pattern.matches("[1-9][0-9]*{0,2}", quantite))
			throw new MissMatchException(
					"La Quanité n'est pas bonne. Insérez une quantité valide!");
		this.quantite = Integer.parseInt(quantite);
	}

	public String toString() {
		return this.getNom();
	}
}
