package model;

public class Achat {

	private Integer idAchat;
	private String nomClient;
	private String prenomClient;
	private String secu;
	private String medecin;
	private String dateAchat;
	private String specialiste;
	private String medicament;
	private String mutuelle;
	private Integer quantiteMedicament;
	private Double prix;
	private Double remboursement;
	private Double prixTotal;

	public Achat(Integer idAchat, String nomClient, String prenomClient, String secu,
			String medecin, String dateAchat, String specialiste, String medicament,
			String mutuelle, Integer quantiteMedicament, Double prix, Double remboursement,
			Double prixTotal) {
		setNomClient(nomClient);
		setPrenomClient(prenomClient);
		setSecu(secu);
		setMedecin(medecin);
		setDateAchat(dateAchat);
		setSpecialiste(specialiste);
		setMedicament(medicament);
		setMutuelle(mutuelle);
		setQuantiteMedicament(quantiteMedicament);
		setPrixTotal(prixTotal);
		setRemboursement(remboursement);
		setPrix(prix);

	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public Double getRemboursement() {
		return remboursement;
	}

	public void setRemboursement(Double remboursement) {
		this.remboursement = remboursement;
	}

	public Integer getIdAchat() {
		return idAchat;
	}

	public void setIdAchat(Integer idAchat) {
		this.idAchat = idAchat;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public String getSecu() {
		return secu;
	}

	public void setSecu(String secu) {
		this.secu = secu;
	}

	public String getMedecin() {
		return medecin;
	}

	public void setMedecin(String medecin) {
		this.medecin = medecin;
	}

	public String getDateAchat() {
		return dateAchat;
	}

	public void setDateAchat(String dateAchat) {
		this.dateAchat = dateAchat;
	}

	public String getSpecialiste() {
		return specialiste;
	}

	public void setSpecialiste(String specialiste) {
		this.specialiste = specialiste;
	}

	public String getMedicament() {
		return medicament;
	}

	public void setMedicament(String medicament) {
		this.medicament = medicament;
	}

	public String getMutuelle() {
		return mutuelle;
	}

	public void setMutuelle(String mutuelle) {
		this.mutuelle = mutuelle;
	}

	public Integer getQuantiteMedicament() {
		return quantiteMedicament;
	}

	public void setQuantiteMedicament(Integer quantiteMedicament) {
		this.quantiteMedicament = quantiteMedicament;
	}

	public Double getPrixTotal() {
		return prixTotal;
	}

	public void setPrixTotal(Double prixTotal) {
		this.prixTotal = prixTotal;
	}

	public String toString() {
		return "Nom: " + nomClient + "\nPrénom: " + prenomClient + "\nN° Sécu: " + secu
				+ "\nMédecin Traitant: " + medecin + "\nDate d'achat: " + dateAchat
				+ "\nSpécialiste: " + specialiste + "\nMédicament: " + medicament + "\nMutuelle: "
				+ mutuelle + "\nQuantité Médicament: " + quantiteMedicament + "\nLe prix total: "
				+ prixTotal;

	}
}