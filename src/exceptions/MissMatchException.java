package exceptions;

public class MissMatchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7921635204325068575L;

	public MissMatchException(String message) {
		super(message);
	}

	public MissMatchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}