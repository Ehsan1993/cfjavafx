package exceptions;

public class NumberFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2686408653436225694L;

	public NumberFormatException() {

	}

	public NumberFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NumberFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NumberFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NumberFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
