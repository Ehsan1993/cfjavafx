
module ProjetCFbyJavaFX {

	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.base;
	requires java.sql;
	requires mysql.connector.java;
	requires java.base;

	opens application to javafx.graphics, javafx.fxml;
	opens model to javafx.base;
}
